// ~~~~~~~LOAD~~~~~~

window.addEventListener("load", function(){
  setTimeout(function(){
    $("#loader").animate({
      opacity:0
    }, 1000, function(){
      hideIt();
    });
  }, 2000);
});

function hideIt(){
  if (document.querySelector("#loader").style.opacity === "0") {
    document.querySelector("#loader").style.display = "none";
  }
}


// ~~~~~~~PARALLAX~~~~~~

$(window).scroll(function(){
  var scrollDist = $(this).scrollTop();
  $(".header-bg").css({
    'transform' : 'translate(0px, -' + scrollDist /50 + '%)'
  });
  $(".landing h1").css({
    'transform' : 'translate(0px, +' + scrollDist /3 + '%)'
  });
  $(".landing h3").css({
    'transform' : 'translate(0px, -' + scrollDist*10 + '%)'
  });

  if ($('#first-about').offset().top -
  ($(window).height()) < scrollDist &&
  $('.second-about').offset().top -
  ($(window).height()*1.4) > scrollDist) {
    console.log("First: " + scrollDist);
    $('#chevron').css({
      'transform' : 'rotate(90deg)'
    });
  }
  else if ($('.second-about').offset().top -
  ($(window).height()) < scrollDist &&
  ($('.second-about').offset().top +
  $('#first-about').offset().top) / 2 > scrollDist) {
      console.log("Second: " + scrollDist);
      $('#chevron').css({
        'transform' : 'rotate(-90deg)'
    });
  }
  // else if ($('.second-about').offset().top +
  // $('#first-about').offset().top < scrollDist) {
  //   $('#chevron').css({
  //     'transform' : 'rotate(0deg)'
  //   });
   else {
    $('#chevron').css({
      'transform' : 'rotate(0deg)'
    });
  }
});

$(window).scroll(function(){
  var scrollDist = $(this).scrollTop();
  if (scrollDist > 50) {
    $('#chevron').css('color', 'black');
  } else {
    $('#chevron').css('color', 'white');
  }
});
